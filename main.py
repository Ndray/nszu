
 file_path = 'test.xlsx'
 import pandas as pd




 df = pd.read_excel(file_path, sheet_name=sheet_name)

 grouped = df.groupby('item_type')

 sum_cost = grouped['cost'].sum().reset_index()
 avg_sum = grouped['cost'].mean().reset_index()

 print("\nЗагальна сума для кожного типу:")
 print(sum_cost)

 print("\nСереднє значення для кожного типу:")
 print(avg_sum)